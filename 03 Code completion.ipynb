{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "# Code completion\n",
    "\n",
    "Now that we're comfortable with the file creation, edit and run loop, let's see what things the IDE can do to help us write better code.\n",
    "\n",
    "One of the most useful every-day abilities that IDEs provide is that of code completion. They read and understand the code you're writing to provide you with suggestions while you're typing. They're not magic and can't write the code for you but they will make it easier to remember the names of functions and what arguments they take for example.\n",
    "\n",
    "Let's start by writing a small script and see how code completion can save on typing. Delete everything in `first.py` and start typing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "%%writefile first.py\n",
    "im"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "You'll immediately that the IDE will pop up a small window below the cursor with a few suggestions in it. You should see `import` as the top option so if you now press the tab key or the return key then it will automatically write the whole word. Depending on which IDE you are using it may automatically write a space charater after the work `import`. If it doesn't, press the space bar to add one."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [
     "pycharm"
    ]
   },
   "source": [
    "\n",
    "![PyCharm Completion](pycharm_completion_im.png \"PyCharm Completion\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "vscode"
    ]
   },
   "source": [
    "\n",
    "![VS Code Completion](vscode_completion_im.png \"VS Code Completion\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Add an `m`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "%%writefile first.py\n",
    "import m"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and you will see the window pop up again. This time it will be giving you a list of modules that you can import. You can see here that it is giving you a list of context-relevant words, not simply any thing it knows about that starts with `m`. It will probably have already selected `math` as the option but if it hasn't then you can either use the cursor keys to move up and down the list or simply keep typing more letters of the thing you want to import until it selects the match you want. Once you have the match selected that you want, press tab or enter again."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [
     "pycharm"
    ]
   },
   "source": [
    "\n",
    "![PyCharm Completion](pycharm_completion_import_m.png \"PyCharm Completion\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "vscode"
    ]
   },
   "source": [
    "\n",
    "![VS Code Completion](vscode_completion_import_m.png \"VS Code Completion\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We've now completed out imports so we can use the function from inside the `math` module. On a new blank link start typing `math` but you'll see that it will again start offering suggestions. Accept the suggestion for `math` and then follow it with a `.`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "%%writefile first.py\n",
    "import math\n",
    "\n",
    "math."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This will offer to you all the functions that are available inside the `math` module."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [
     "pycharm"
    ]
   },
   "source": [
    "\n",
    "![PyCharm Completion](pycharm_completion_math.png \"PyCharm Completion\")\n",
    "\n",
    "If you want to see a preview of the documentation for each function as you scroll through the list press `Ctrl`+`Q` (`F1` on macOS). This will show an extra pop-up on the right-hand side of the function list with a summary of the documentation for that function.\n",
    "\n",
    "To turn this on permanently, go to `File` → `Settings...`/`Preferences...` and then `Editor` → `General` → `Code Completion` and check the `Show the documentation popup` box. Also see [the PyCharm documentation for this](https://www.jetbrains.com/help/pycharm/inline-documentation.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "vscode"
    ]
   },
   "source": [
    "\n",
    "![VS Code Completion](vscode_completion_math.png \"VS Code Completion\")\n",
    "\n",
    "As you browse through the list of functions, VS Code will automatically pop up a short snippet of the documentation for that function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Either scroll through the list for `ceil` or type more letters of the name until it is selected and press enter or tab. It will auto-complete the name and in some IDEs it will automatically place round brackets `()` after the name as it has recognised it as a function. If it hasn't done so automatically, type `(` and it will also create the `)` and put your cursor in between them.\n",
    "\n",
    "As your cursor sits between the brackets it will pop up a small window giving you a hint as to the parameter it is expecting. In this case it will say something like `x: SupportsFloat` which means that the parameter is called `x` and it is of the type [`SupportsFloat`](https://docs.python.org/3/library/typing.html?#typing.SupportsFloat) so it will accept integers and floats."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "pycharm"
    ]
   },
   "source": [
    "\n",
    "![PyCharm Completion](pycharm_completion_supportsfloat.png \"PyCharm Completion\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "vscode"
    ]
   },
   "source": [
    "\n",
    "![VS Code Completion](vscode_completion_supportsfloat.png \"VS Code Completion\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Type `5.4` between the brackets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "%%writefile first.py\n",
    "import math\n",
    "\n",
    "math.ceil(5.4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "of course, calling this function by itself does not do anything useful so let's assign the return value to a variable and print it out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "%%writefile first.py\n",
    "import math\n",
    "\n",
    "a = math.ceil(5.4)\n",
    "\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you get more used to using code completion you will find it very neatly fits into your workflow and can considerably speed up your coding. I also find it allows me to feel comfortable using long, descriptive variable names since I know that I won't have to type them all out manually."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Call some other functions from the `math` module, using code completion and the documentation hints to guide you. Assign them to variables and print them. Run the script to check the output."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
