{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "# Running tests\n",
    "\n",
    "Testing your code is a very important part of the development process. If you want to read more into the background of testing and learn more about the feautes available in Python, take a look at the [testing chapter of the Best Practices in Software Engineering course](https://milliams.com/courses/software_engineering_best_practices/Testing.html).\n",
    "\n",
    "For now, we'll assume we know how to write tests and will instead see how the IDE can make running them easier.\n",
    "\n",
    "The first thing we need to do is enable the integration feature in the IDE."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [
     "pycharm"
    ]
   },
   "source": [
    "1. Open `File` → `Settings...`/`Preferences...`. Go to `Tools` → `Python Integrated Tools` in the area on the left.\n",
    "2. In the `Default test runner` field select `pytest`.\n",
    "3. Click `Apply` to save the settings.\n",
    "\n",
    "If \"No pytest runner found in selected interpreter\" shows up in that window near the bottom then choose `Fix`. This will install pytest in the background.\n",
    "\n",
    "Press Ok to close the settings window.\n",
    "\n",
    "You can see more information on this in the [official documentation](https://www.jetbrains.com/help/pycharm/pytest.html#enable-pytest)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "vscode"
    ]
   },
   "source": [
    "1. Go to `View` → `Command Palette...`\n",
    "2. Search for `Python: Configure Tests` and press enter\n",
    "3. Select `pytest`\n",
    "4. Select `. Root directory` when asked to select the directory containing the tests\n",
    "5. A popup will ask `Test framework pytest is not installed. Install?` Choose `Yes`\n",
    "\n",
    "You can see more information on this in the [official documentation](https://code.visualstudio.com/docs/python/testing)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running a single test\n",
    "\n",
    "Now that testing is enabled we can go ahead and write our first test. A test setup is is made of two parts: the code to be tested and the code to do the testing. We'll have to create both.\n",
    "\n",
    "First the code to be tested in `utils.py`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "%%writefile utils.py\n",
    "def add_arrays(x, y):\n",
    "    \"\"\"\n",
    "    This function adds together each element of the two passed lists.\n",
    "\n",
    "    Args:\n",
    "        x (list): The first list to add\n",
    "        y (list): The second list to add\n",
    "\n",
    "    Returns:\n",
    "        list: the pairwise sums of ``x`` and ``y``.\n",
    "\n",
    "    Examples:\n",
    "        >>> add_arrays([1, 4, 5], [4, 3, 5])\n",
    "        [5, 7, 10]\n",
    "    \"\"\"\n",
    "    z = []\n",
    "    for x_, y_ in zip(x, y):\n",
    "        z.append(x_ + y_)\n",
    "\n",
    "    return z"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then make another file called `test_utils.py` which will contain our testing code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "%%writefile test_utils.py\n",
    "from utils import add_arrays\n",
    "\n",
    "\n",
    "def test_add_arrays():\n",
    "    a = [1, 2, 3]\n",
    "    b = [4, 5, 6]\n",
    "    expect = [5, 7, 9]\n",
    "\n",
    "    output = add_arrays(a, b)\n",
    "\n",
    "    assert output == expect"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that our code is all in place, we can go ahead and run the test."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [
     "pycharm"
    ]
   },
   "source": [
    "As soon as PyCharm recognises that this is a test, there should be a green \"<span style=\"color:green;\">⏵</span>\" button next to the function name.\n",
    "\n",
    "![PyCharm run test](pycharm_test_1.png \"PyCharm run test\")\n",
    "\n",
    "Click it and select `Run 'pytest for test_util....'`\n",
    "\n",
    "![PyCharm run test_util](pycharm_test_2.png \"PyCharm run test_util\")\n",
    "\n",
    "This will open up a new tab at the bottom of the screen with two panes. The left-hand one containing the list of tests and the pane on the right containing the output of pytest. You can read more about this tab in the [official documentation](https://www.jetbrains.com/help/pycharm/test-runner-tab.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [
     "vscode"
    ]
   },
   "source": [
    "Above the name of the function , you should see `Run Test`.\n",
    "\n",
    "![VS Code run test](vscode_test_1.png \"VS Code run test\")\n",
    "\n",
    "If you don't see the `Run Test` link above the test name then you may see `⚡ Test discovery failed` in the bottom status bar. If so click it and it try to find the tests again. If you don't see `⚡ Test discovery failed` either then open the command palette (`View`→`Command Palette...`) and find and run `Python: Discover Tests`.\n",
    "\n",
    "Click `Run Test` and it should change to `✔ Run Test` which means that the test has passed successfully.\n",
    "\n",
    "![VS Code ran test](vscode_test_2.png \"VS Code ran test\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running all tests\n",
    "\n",
    "Running a single test is useful while you are developing it or the code which it is testing but you will often want to run all the tests in the whole project to make sure that something else hasn't been broken.\n",
    "\n",
    "Add a second test to `test_utils.py`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "%%writefile test_utils.py\n",
    "from utils import add_arrays\n",
    "\n",
    "\n",
    "def test_add_arrays():\n",
    "    a = [1, 2, 3]\n",
    "    b = [4, 5, 6]\n",
    "    expect = [5, 7, 9]\n",
    "\n",
    "    output = add_arrays(a, b)\n",
    "\n",
    "    assert output == expect\n",
    "\n",
    "\n",
    "def test_add_empty_arrays():\n",
    "    assert add_arrays([], []) == []"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [
     "pycharm"
    ]
   },
   "source": [
    "In the project browser pane on the left-hand side of the screen, right-click on the name of the folder `utils` and select `Run 'pytest in utils'`. This will run both of the tests and show the output in the `Run` tab at the bottom of the screen."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [
     "vscode"
    ]
   },
   "source": [
    "On the far left-hand side of the screen there is a column of icons. Click on the conical flask icon (when hovered it says `Test`). This opens the testing tab.\n",
    "\n",
    "At the top of that pane there is a green play button.\n",
    "\n",
    "![VS Code run all tests](vscode_test_runall.png \"VS Code run all tests\")\n",
    "\n",
    "Press that button which will run all tests in the project. You should see the list of items below the button change briefly to a ↻ icon and then to a green tick:\n",
    "\n",
    "![VS Code run summary](vscode_test_run_summary.png \"VS Code run summary\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Failing tests\n",
    "\n",
    "In the situation where all the tests are passing (hopefully most of the time) the IDE will simplify what it shows you since there's not much more to say. However, if a test fails you will be given more information.\n",
    "\n",
    "Let's deliberately break one of the tests to simulate this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "%%writefile test_utils.py\n",
    "from utils import add_arrays\n",
    "\n",
    "\n",
    "def test_add_arrays():\n",
    "    a = [1, 2, 3]\n",
    "    b = [4, 5, 6]\n",
    "    expect = [5, 7, 999]  # ← This was changed from 9 to 999\n",
    "\n",
    "    output = add_arrays(a, b)\n",
    "\n",
    "    assert output == expect\n",
    "\n",
    "\n",
    "def test_add_empty_arrays():\n",
    "    assert add_arrays([], []) == []"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run all the tests again."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [
     "pycharm"
    ]
   },
   "source": [
    "PyCharm allows you to easily rerun whatever you ran previously. This applies to running scripts, debugging scripts and running tests.\n",
    "\n",
    "In the top-right corner of the screen there are the run widgets:\n",
    "\n",
    "![PyCharm run configuration](pycharm_test_run_configuration.png \"PyCharm run configuration\")\n",
    "\n",
    "The first thing is the drop-down which lets you reselect previous runs as well as create new configurations to run whatever script or tool you wish. Next to that is the green \"play\" button which reruns the last configuration. Next is the \"insect\" debug button which runs the same configuration but through the debugger. Finally there is a red stop button (greyed-out be default) which allows you to forcibly stop any running task.\n",
    "\n",
    "This drop-down is part of a very powerful run configuration system in PyCharm. We don't need most of its power, but it's good to know where to find it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see the fact that the test failed in the test summary pane in the IDE."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [
     "pycharm"
    ]
   },
   "source": [
    "\n",
    "![PyCharm test fail](pycharm_test_run_fail.png \"PyCharm test fail\")\n",
    "\n",
    "The information about how the test failed is in the right-hand pane. If you expand out `test_utils` in the summary list by clicking the arrow next to it, you can then click each entry to see the results for just that test.\n",
    "\n",
    "![PyCharm test fail](pycharm_test_fail_details.png \"PyCharm test fail\")\n",
    "\n",
    "By default, tests that passed are hidden in this list. To show them, press the tick button shown in the top-left of the above screenshot."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [
     "vscode"
    ]
   },
   "source": [
    "\n",
    "![VS Code test fail](vscode_test_run_fail.png \"VS Code test fail\")\n",
    "\n",
    "In the code editor pane the test name will now have a red underline beneath it and `✔ Run Test` will have changed to `✘ Run Test`. Hover over the name of the test and you will see a popup with the details of how the test failed.\n",
    "\n",
    "![VS Code test fail](vscode_test_fail_details.png \"VS Code test fail\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "Before continuing, fix that test by changing it back to `9`."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
